package com.example.mvvmexample

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvmexample.databinding.ListMainBinding

class Adapter(private val mylist:MutableList<MyModel>):RecyclerView.Adapter<Adapter.holder>() {


     override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        val binding:ListMainBinding=DataBindingUtil.inflate(LayoutInflater.from(parent.context),R.layout.list_main,parent,false)
         return  holder(binding)
     }

    override fun getItemCount(): Int =mylist.size

    override fun onBindViewHolder(holder: holder, position: Int) {
        holder.onbind()
     }

    inner class holder(val binding:ListMainBinding): RecyclerView.ViewHolder(binding.root){

        fun onbind(){
            binding.user=mylist[adapterPosition]
        }

    }

}