package com.example.mvvmexample.Interfaces

interface CallbackApi {
    fun onResponse(value:String?)
    fun onFailure(value:String?)
}